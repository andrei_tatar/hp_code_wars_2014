#include <stdlib.h>
#include <Windows.h>

#include "diskspace.h"

long get_root_freespace (void)
{
	BOOL res;
	unsigned __int64 myFreeBytes, totalBytes, freeBytes;
	res = GetDiskFreeSpaceEx((LPCTSTR)_wgetenv(L"SystemDrive"),
							 (PULARGE_INTEGER)&myFreeBytes,
							 (PULARGE_INTEGER)&totalBytes,
							 (PULARGE_INTEGER)&freeBytes);
	if (!res)
	{
		return (unsigned long) freeBytes;
	}
	else
	{
		return -1;
	}
}