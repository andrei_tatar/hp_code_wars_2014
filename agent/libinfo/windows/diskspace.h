/*
 * diskspace.h
 */

#ifndef _WIN32DISKSPACE_H
#define _WIN32DISKSPACE_H

long get_root_freespace (void);

#endif