/*
 * process.h
 */

#ifndef _WIN32PROCESS_H
#define _WIN32PROCESS_H

struct win32_procinfo {
    unsigned short procsno;
};

int get_win32_procinfo (struct win32_procinfo *proc);

#endif