#include <Windows.h>

#include "ram.h"

int get_win32_ram (struct win32_ram *wram)
{
	MEMORYSTATUSEX memst;
	memst.dwLength = sizeof(memst);
	if (GlobalMemoryStatusEx((LPMEMORYSTATUSEX)&memst))
	{
		wram->totalram = (unsigned long) memst.ullTotalPhys;
		wram->freeram = (unsigned long) memst.ullAvailPhys;
		wram->totalpagefile = (unsigned long) memst.ullTotalPageFile;
		wram->freepagefile = (unsigned long) memst.ullAvailPageFile;
		return 0;
	}
	else
	{
		return -1;
	}
}