/*
 * process.h
 */

#ifndef _LINUXPROCESS_H
#define _LINUXPROCESS_H

struct linux_procinfo {
    unsigned short procsno;
};

int get_linux_procinfo (struct linux_procinfo * lproc);

#endif
