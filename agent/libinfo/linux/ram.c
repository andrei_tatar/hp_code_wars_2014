#include <string.h>
#include <stdio.h>
#include <sys/sysinfo.h>

#include "ram.h"

int get_linux_ram (struct linux_ram *lram)
{
    struct sysinfo info;
    if (!sysinfo(&info))
    {
        memcpy(lram, &(info.totalram), sizeof(lram));
        return 0;
    }
    else
    {
        return -1;
    }
}
