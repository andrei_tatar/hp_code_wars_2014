#include <stdio.h>
#include "info.h"

/* Helper debug code */

unsigned long get_freeram()
{
	struct raminfo info;
	return (!get_raminfo(&info)) ? info.freeram : 0;
}

unsigned long get_totalram()
{
	struct raminfo info;
	return (!get_raminfo(&info)) ? info.totalram : 0;
}

unsigned short get_processcount()
{
	struct procinfo info;
	return (!get_procinfo(&info)) ? info.processcount : 0;
}

/* End helper debug code */

void put_separator (const char * msg)
{
	printf("<------------------------------------------>\n");
	printf("%s\n", msg);
	printf("<------------------------------------------>\n");
}

int main (int argc, char *argv[])
{
	unsigned short i;
	put_separator("RUNNING TESTS");
	put_separator("RAM");
	printf("Total RAM: %lu ; Free RAM: %lu\n", get_totalram(), get_freeram());
	#ifdef __linux__
	put_separator("output of free");
	system("free -b");
	#endif
	put_separator("PROCESSES");
	i = get_processcount();
	printf("Number of processes: %d\n", i);
	#ifdef __linux__
	printf("Expected for 'ps' comparison: %d\n", i+4);
	put_separator("output of 'ps axHo lwp | wc -l'");
	system("ps axHo lwp | wc -l");
	#endif
	put_separator("FREE DISK SPACE");
	printf("Free space (in Kbytes): %lu\n", get_system_freespace() >> 10);
	#ifdef __linux__
	put_separator("output of 'df -BK'");
	system("df -BK /");
	#endif
	put_separator("DONE");
	return 0;
}
