#!/usr/bin/env python

"""Wrap the functionality of libinfo.so in the Libinfo class"""

import ctypes
import struct
import platform


class Libinfo(object):

    def __init__(self):
        if platform.system() == 'Windows':
            self.lib = ctypes.cdll.libinfo
        else:
            self.lib = ctypes.cdll.LoadLibrary('libinfo.so')
        self.raminfo_buf = ctypes.create_string_buffer(2 * ctypes.sizeof(ctypes.c_ulong))
        self.procinfo_buf = ctypes.create_string_buffer(ctypes.sizeof(ctypes.c_ushort))
        self.get_raminfo_f = self.lib.get_raminfo
        self.get_procinfo_f = self.lib.get_procinfo
        self.get_system_freespace_f = self.lib.get_system_freespace
        self.get_system_freespace_f.restype = ctypes.c_long

    def get_raminfo(self):
        ret = self.get_raminfo_f(self.raminfo_buf)
        if ret == 0:
            return struct.unpack('Ll', self.raminfo_buf.raw)
        else:
            raise OSError('Library call failed')

    def get_procinfo(self):
        ret = self.get_procinfo_f(self.procinfo_buf)
        if ret == 0:
            return struct.unpack('H', self.procinfo_buf.raw)
        else:
            raise OSError('Library call failed')

    def get_system_freespace(self):
        ret = self.get_system_freespace_f()
        if ret >= 0:
            return ret
        else:
            raise OSError('Library call failed')
