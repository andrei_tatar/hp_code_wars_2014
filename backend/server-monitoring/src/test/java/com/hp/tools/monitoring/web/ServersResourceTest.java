package com.hp.tools.monitoring.web;

import com.hp.tools.monitoring.entities.Server;
import com.hp.tools.monitoring.entities.ServerDetails;
import com.hp.tools.monitoring.entities.ServerHistory;
import com.hp.tools.monitoring.util.DateTimeAdapter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.jetty.JettyTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainer;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.joda.time.DateTime;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
public class ServersResourceTest extends JerseyTest {

    public ServersResourceTest() throws TestContainerException {
        super(new JettyTestContainerFactory());
    }

    @Override
    protected ResourceConfig configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        enable("JacksonFeature");

        return new ResourceConfig()
                .register(ServersResource.class)
                .register(JacksonFeature.class);
    }

    @Override
    protected Client getClient(TestContainer tc, ApplicationHandler applicationHandler) {
        Client client = super.getClient(tc, applicationHandler);
        client.register(JacksonFeature.class);
        return client;
    }

    @Test
    public void testGetEmptyServerHistory() throws Exception {
        Server server = new Server("valid-id", "Test PC");
        Response response = target("/servers").request().post(Entity.entity(server, MediaType.APPLICATION_JSON_TYPE));
        assertEquals(response.getStatus(), 201); //Created

        String history = target("/servers/valid-id/history").request(MediaType.APPLICATION_JSON_TYPE).get(String.class);
        assertEquals(history, "[]");

        Server createdServer = target("/servers/valid-id").request(MediaType.APPLICATION_JSON_TYPE).get(Server.class);
        assertEquals(server.getId(), createdServer.getId());
        assertEquals(server.getName(), createdServer.getName());
    }

    @Test
    public void testGetNotEmptyServerHistory() throws Exception {
        Server server = new Server("valid-id", "Test PC");
        Response response = target("/servers").request().post(Entity.entity(server, MediaType.APPLICATION_JSON_TYPE));
        assertEquals(response.getStatus(), 201); //Created

        ServerDetails details = new ServerDetails(DateTime.now(), 1, 2, 3, 4);
        response = target("/servers/valid-id/detail").request().put(Entity.entity(details, MediaType.APPLICATION_JSON_TYPE));
        assertEquals(response.getStatus(), 202); //Accepted
        checkServerDetails(details, response.readEntity(ServerDetails.class));

        List<ServerDetails> detailsList = target("/servers/valid-id/history").request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<ServerDetails>>() {
        });
        assertEquals(detailsList.size(), 1);
        checkServerDetails(details, detailsList.get(0));

        List<ServerHistory> historyList = target("/servers/history").request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<ServerHistory>>() {
        });
        assertEquals(historyList.size(), 1);
        checkServerDetails(details, historyList.get(0).getHistory().get(0));
    }

    private void checkServerDetails(ServerDetails before, ServerDetails after) throws Exception {
        assertEquals(before.getDiskFreeSpace(), after.getDiskFreeSpace());
        assertEquals(before.getMemoryFreeSpace(), after.getMemoryFreeSpace());
        assertEquals(before.getMemoryTotalSize(), after.getMemoryTotalSize());
        assertEquals(before.getProcessesNumber(), after.getProcessesNumber());

        DateTimeAdapter adapter = new DateTimeAdapter();
        String s1 = adapter.marshal(before.getTimestamp());
        String s2 = adapter.marshal(after.getTimestamp());
        assertEquals(s1, s2);
    }

    @Test
    public void testGetEmptyHistories() throws Exception {
        String response = target("/servers/history").request(MediaType.APPLICATION_JSON_TYPE).get(String.class);
        assertEquals(response, "[]");
    }
}
