package com.hp.tools.monitoring.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
public class DateTimeAdapterTest {
    private DateTimeAdapter adapter;

    @Before
    public void setUp() throws Exception {
        adapter = new DateTimeAdapter();
    }

    @After
    public void tearDown() throws Exception {
        adapter = null;
    }

    @Test
    public void testUnmarshal() throws Exception {
        DateTime dateTime = adapter.unmarshal("2014-02-11T10:22:46.000Z");

        assertEquals(dateTime.getDayOfMonth(), 11);
        assertEquals(dateTime.getMonthOfYear(), 2);
        assertEquals(dateTime.getYear(), 2014);
        assertEquals(dateTime.getHourOfDay(), 10);
        assertEquals(dateTime.getMinuteOfHour(), 22);
        assertEquals(dateTime.getSecondOfMinute(), 46);
        assertEquals(dateTime.getZone().getOffset(dateTime), 0);
    }

    @Test
    public void testMarshal() throws Exception {
        DateTime dateTime = new DateTime(2014, 2, 11, 10, 22, 46, DateTimeZone.UTC);
        String dateStr = adapter.marshal(dateTime);

        assertEquals(dateStr, "2014-02-11T10:22:46.000Z");
    }

    @Test
    public void testUnmarshalMarshal() throws Exception {
        String after = "2014-02-11T10:22:46.000Z";
        String before = adapter.marshal(adapter.unmarshal(after));

        assertEquals(after, before);
    }
}
