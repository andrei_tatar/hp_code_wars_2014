package com.hp.tools.monitoring.services;

import com.hp.tools.monitoring.entities.Server;
import com.hp.tools.monitoring.entities.ServerDetails;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
public class MonitoringServiceTest {
    private MonitoringService service;

    @Before
    public void setUp() throws Exception {
        service = new MonitoringService();
    }

    @After
    public void tearDown() throws Exception {
        service = null;
    }

    @Test
    public void testGetInvalidServer() throws Exception {
        assertNull(service.getServer("invalid-id"));
    }

    @Test
    public void testGetValidServer() throws Exception {
        service.addServer(new Server("valid-id", "Test PC"));
        Server server = service.getServer("valid-id");

        assertNotNull(server);
        assertEquals(server.getName(), "Test PC");
    }

    @Test
    public void testUpdateValidServerDetails() throws Exception {
        service.addServer(new Server("valid-id", "Test PC"));
        service.updateServerDetails("valid-id", new ServerDetails(DateTime.now(), 1, 2, 3, 4));
        List<ServerDetails> details = service.getServerHistory("valid-id");

        assertNotNull(details);
        assertEquals(details.size(), 1);
        assertNotNull(details.get(0));
        assertEquals(details.get(0).getProcessesNumber(), 2);
    }

    @Test
    public void testUpdateInvalidServerDetails() throws Exception {
        service.updateServerDetails("invalid-id", new ServerDetails(DateTime.now(), 1, 2, 3, 4));

        assertNull(service.getServer("invalid-id"));
        assertEquals(service.getHistories().size(), 0);
    }

    @Test
    public void testGetInvalidServerHistory() throws Exception {
        assertNull(service.getServerHistory("invalid-id"));
    }

    @Test
    public void testGetEmptyServersHistory() throws Exception {
        assertNotNull(service.getHistories());
        assertEquals(service.getHistories().size(), 0);
    }
}
