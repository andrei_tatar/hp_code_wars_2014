package com.hp.tools.monitoring.entities;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
@XmlRootElement
public class Server {
    private String id;
    private String name;

    /**
     * Used by JAXB.
     */
    public Server() {
    }

    public Server(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
