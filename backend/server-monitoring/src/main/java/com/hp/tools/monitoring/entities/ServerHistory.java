package com.hp.tools.monitoring.entities;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
@XmlRootElement
public class ServerHistory {
    private Server server;
    private List<ServerDetails> history;

    /**
     * Used by JAXB.
     */
    public ServerHistory() {
    }

    public ServerHistory(Server server) {
        this.server = server;
        this.history = new ArrayList<>();
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public List<ServerDetails> getHistory() {
        return history;
    }

    public void setHistory(List<ServerDetails> history) {
        this.history = history;
    }

    public void addServerDetails(ServerDetails serverDetails) {
        history.add(serverDetails);
    }
}
