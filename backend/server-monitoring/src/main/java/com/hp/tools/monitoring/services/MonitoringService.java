package com.hp.tools.monitoring.services;

import com.hp.tools.monitoring.entities.Server;
import com.hp.tools.monitoring.entities.ServerDetails;
import com.hp.tools.monitoring.entities.ServerHistory;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
public class MonitoringService {
    private HashMap<String, ServerHistory> histories;

    public MonitoringService() {
        histories = new HashMap<>();
    }

    public Server getServer(String serverId) {
        ServerHistory history = histories.get(serverId);
        if (history != null) {
            return history.getServer();
        }
        return null;
    }

    public boolean addServer(Server server) {
        ServerHistory history = histories.get(server.getId());
        if (history == null) {
            history = new ServerHistory(server);
            histories.put(server.getId(), history);
            return true;
        }

        return false;
    }

    public boolean updateServerDetails(String serverId, ServerDetails serverDetails) {
        ServerHistory history = histories.get(serverId);
        if (history != null) {
            if (serverDetails.getTimestamp() == null) {
                serverDetails.setTimestamp(DateTime.now());
            }
            history.addServerDetails(serverDetails);
            return true;
        }

        return false;
    }

    public List<ServerDetails> getServerHistory(String serverId) {
        ServerHistory history = histories.get(serverId);
        if (history != null) {
            return histories.get(serverId).getHistory();
        }
        return null;
    }

    public List<ServerHistory> getHistories() {
        return new ArrayList<>(histories.values());
    }
}
